<?php

/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728096
 */

function queerlibrary_preprocess_html(&$variables) {
    if (arg(2)=='edit' || (arg(0)=='user' && (arg(1)=='register' || arg(3)=='edit'))) {       
       $variables['classes_array'][]='two-sidebars';
       $variables['classes_array'][]='insideregions';

    }
    if (arg(0)=='taxonomy' && arg(1)=='term' && (arg(3)=='edit') || arg(3)=='add') {       
       
       $variables['classes_array'][]='two-sidebars';
       $variables['classes_array'][]='insideregions';

    }
    if (
            (arg(0) == 'node' && arg(1) == 'add') ||
            (arg(0) == 'node' && is_numeric(arg(1)) && arg(2) == 'edit')
        ) {
       $variables['classes_array'][]='two-sidebars';
       $variables['classes_array'][]='insideregions';
    }

    // CSS für Galerie
    if (arg(0)=='node' && arg(1)=='89') {
        drupal_add_css('galerie/css/bootstrap_layout.css', array('group' => CSS_THEME, 'every_page' => FALSE));
        drupal_add_css('galerie/css/lightbox.css', array('group' => CSS_THEME, 'every_page' => FALSE));
        drupal_add_css('galerie/css/custom_galerie_2.css', array('group' => CSS_THEME, 'every_page' => FALSE));
    }

}

function queerlibrary_preprocess_page(&$variables) {

    if (empty($variables['page']['sidebar_first'])) {
        $variables['classes_array'][] = 'nosidebar';
    }
   /* if (!$GLOBALS['user']->uid == 1 && arg(0) != 'user')
        unset($variables['tabs']);
    if (arg(0) == 'user')
        unset($variables['tabs']);*/
    //var_dump($variables['classes_array']); 
    if ($variables['theme_hook_suggestions'][0]=='page__nocolumns') {
        $variables['classes_array'][] = 'two-sidebars';
    }
    
   // page__nocolumns
// && arg(1)!=$GLOBALS['user']->uid // && arg(1)!=$GLOBALS['user']->uid
}

function queerlibrary_date_combo($variables) {

    return theme('form_element', $variables);
}


function queerlibrary_select_as_checkboxes($vars) {

    $element = $vars['element'];
    if (!empty($element['#bef_nested'])) {
        if (empty($element['#attributes']['class'])) {
            $element['#attributes']['class'] = array();
        }
        $element['#attributes']['class'][] = 'form-checkboxes';
        return theme('select_as_tree', array('element' => $element));
    }

    // the selected keys from #options
    $selected_options = empty($element['#value']) ? $element['#default_value'] : $element['#value'];

    // Grab exposed filter description.  We'll put it under the label where it makes more sense.
    $description = '';
    if (!empty($element['#bef_description'])) {
        $description = '<div class="description">' . $element['#bef_description'] . '</div>';
    }

    $output = '<div class="bef-checkboxes">';
    foreach ($element['#options'] as $option => $elem) {
        if ('All' === $option) {
            // TODO: 'All' text is customizable in Views
            // No need for an 'All' option -- either unchecking or checking all the checkboxes is equivalent
            continue;
        }

        // Check for Taxonomy-based filters
        if (is_object($elem)) {
            list($option, $elem) = each(array_slice($elem->option, 0, 1, TRUE));
        }

        /*
         * Check for optgroups.  Put subelements in the $element_set array and add a group heading.
         * Otherwise, just add the element to the set
         */
        $element_set = array();
        $is_optgroup = FALSE;
        if (is_array($elem)) {
            $output .= '<div class="bef-group">';
            $output .= '<div class="bef-group-heading">' . $option . '</div>';
            $output .= '<div class="bef-group-items">';
            $element_set = $elem;
            $is_optgroup = TRUE;
        } else {
            $element_set[$option] = $elem;
        }

        foreach ($element_set as $key => $value) {
            if ($element['#id'] != 'edit-field-lgbtiaqf-tid') {
                $output .= bef_checkbox($element, $key, $value, array_search($key, $selected_options) !== FALSE);
            } else {
                $output .= ql_bef_checkbox($element, $key, $value, array_search($key, $selected_options) !== FALSE);
            }
        }

        if ($is_optgroup) {
            $output .= '</div></div>'; // Close group and item <div>s
        }
    }
    $output .= '</div>';

    // Fake theme_checkboxes() which we can't call because it calls theme_form_element() for each option
    $attributes['class'] = array('form-checkboxes', 'bef-select-as-checkboxes');
    if (!empty($element['#bef_select_all_none'])) {
        $attributes['class'][] = 'bef-select-all-none';
    }
    if (!empty($element['#attributes']['class'])) {
        $attributes['class'] = array_merge($element['#attributes']['class'], $attributes['class']);
    }

    return '<div' . drupal_attributes($attributes) . ">$description$output</div>";
}

function ql_bef_checkbox($element, $value, $label, $selected) {
    $value = check_plain($value);
    $label = check_plain($label);
    $id = drupal_html_id($element['#id'] . '-' . $value);
    // Custom ID for each checkbox based on the <select>'s original ID
    $properties = array(
        '#required' => FALSE,
        '#id' => $id,
        '#type' => 'bef-checkbox',
        '#name' => $id,
    );

    // Prevent the select-all-none class from cascading to all checkboxes
    if (!empty($element['#attributes']['class']) && FALSE !== ($key = array_search('bef-select-all-none', $element['#attributes']['class']))) {
        unset($element['#attributes']['class'][$key]);
    }
    $term = taxonomy_term_load($value);
    $title = $term->description;

    $checkbox = '<input type="checkbox" '
            . 'name="' . $element['#name'] . '[]" '    // brackets are key -- just like select
            . 'id="' . $id . '" '
            . 'value="' . $value . '" '
            . ($selected ? 'checked="checked" ' : '')
            . drupal_attributes($element['#attributes']) . ' />';
    $properties['#children'] = "$checkbox <label class='option' for='$id' title='$title'>$label</label>";
    $output = theme('form_element', array('element' => $properties));
    return $output;
}

function queerlibrary_form_element_label($variables) {
  $element = $variables['element'];

  // This is also used in the installer, pre-database setup.
  $t = get_t();

  // If title and required marker are both empty, output no label.
  if ((!isset($element['#title']) || $element['#title'] === '') && empty($element['#required'])) {
    return '';
  }
  

  // If the element is required, a required marker is appended to the label.
  $required = !empty($element['#required']) ? theme('form_required_marker', array('element' => $element)) : '';

  $title = filter_xss_admin($element['#title']);

  $attributes = array();
  $attributes['class']='';
  if ($element['#parents'][0]=='field_lgbtiaqf') {
      if (isset($element['#return_value'])) {
        $term=taxonomy_term_load($element['#return_value']);
        $attributes['title']=$term->description;
        $attributes['class']='shortlabel ';
      } 
  }
  // Style the label as class option to display inline with the element.
  if ($element['#title_display'] == 'after') {
    $attributes['class'] .= 'option';
  }
  // Show label only to screen readers to avoid disruption in visual flows.
  elseif ($element['#title_display'] == 'invisible') {
    $attributes['class'] .= 'element-invisible';
  }

  if (!empty($element['#id'])) {
    $attributes['for'] = $element['#id'];
  }

  // The leading whitespace helps visually separate fields from inline labels.
  return ' <label' . drupal_attributes($attributes) . '>' . $t('!title !required', array('!title' => $title, '!required' => $required)) . "</label>\n";
}

function queerlibrary_menu_link(array $variables) {
    $element = $variables['element'];
    switch ($element['#href']) {
        case 'user':
            $element['#href']='user/me';
            if ($GLOBALS['user']->uid != 0) {
                $link['title'] = $GLOBALS['user']->name;
                // manually prepare picture, <a> tag with image reference, username (theme_user_picture won't work due to nested <a> tag)
                if (!empty($GLOBALS['user']->picture)) {
                    $fid = $GLOBALS['user']->picture;
                    $file = file_load($fid);
                    $element['#title'] = theme('image_style', array('style_name' => 'tiny', 'path' => $file->uri, 'alt' => $link['title'], 'title' => $link['title'])) . $link['title'] . " ";
                    $element['#localized_options']['html'] = TRUE;
                    $element['#localized_options']['attributes']['class'][] = 'account';
                }
            } else {
                $link['#title'] = 'Login | Registrieren';
                $element['#localized_options']['attributes']['class'][] = 'login';
            }
            break;
        case 'user/logout';
            $link['#title'] = '&#x2716;';
            $link['#html'] = TRUE;
            $element['#localized_options']['attributes']['class'][] = 'logout';
            break;
    }
    
    

  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";

}

function queerlibrary_textarea($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id', 'name', 'cols', 'rows', 'placeholder'));
  _form_set_class($element, array('form-textarea'));

  $wrapper_attributes = array(
    'class' => array('form-textarea-wrapper'),
  );
  if (!isset($element['#description'])) $element['#description']="";
  // Add resizable behavior.
  if (!empty($element['#resizable'])) {
    drupal_add_library('system', 'drupal.textarea');
    $wrapper_attributes['class'][] = 'resizable';
  }
  $output = '<div' . drupal_attributes($wrapper_attributes) . '>';
  $output .= '<textarea' . drupal_attributes($element['#attributes']) .'">' . check_plain($element['#value']) . '</textarea>';
  $output .= '</div>';
  unset($element['#description']);
  return $output;
}

/**
 * Override or insert variables into the maintenance page template.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("maintenance_page" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_maintenance_page(&$variables, $hook) {
  // When a variable is manipulated or added in preprocess_html or
  // preprocess_page, that same work is probably needed for the maintenance page
  // as well, so we can just re-use those functions to do that work here.
  STARTERKIT_preprocess_html($variables, $hook);
  STARTERKIT_preprocess_page($variables, $hook);
}
// */

/**
 * Override or insert variables into the html templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_html(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // The body tag's classes are controlled by the $classes_array variable. To
  // remove a class from $classes_array, use array_diff().
  //$variables['classes_array'] = array_diff($variables['classes_array'], array('class-to-remove'));
}
// */

/**
 * Override or insert variables into the page templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_page(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the node templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_node(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // Optionally, run node-type-specific preprocess functions, like
  // STARTERKIT_preprocess_node_page() or STARTERKIT_preprocess_node_story().
  $function = __FUNCTION__ . '_' . $variables['node']->type;
  if (function_exists($function)) {
    $function($variables, $hook);
  }
}
// */

/**
 * Override or insert variables into the comment templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_comment(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the region templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("region" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_region(&$variables, $hook) {
  // Don't use Zen's region--sidebar.tpl.php template for sidebars.
  //if (strpos($variables['region'], 'sidebar_') === 0) {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('region__sidebar'));
  //}
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_block(&$variables, $hook) {
  // Add a count to all the blocks in the region.
  // $variables['classes_array'][] = 'count-' . $variables['block_id'];

  // By default, Zen will use the block--no-wrapper.tpl.php for the main
  // content. This optional bit of code undoes that:
  //if ($variables['block_html_id'] == 'block-system-main') {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('block__no_wrapper'));
  //}
}
// */
