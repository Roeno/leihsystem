<?php

/**
 * @file
 * Default theme implementation for a reply.
 */

?>
<article id="reply-<?php print $id ?>" class="<?php print $classes ?>">
  <div class="reply-body"><?php print render($content) ?></div>
  <div class="reply-links"><?php print render($links) ?></div>
  <div class="reply-time"><?php print date("d.m.Y H:i",$reply->changed);?></div>
</article>
