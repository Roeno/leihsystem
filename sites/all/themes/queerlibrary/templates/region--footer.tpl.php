<?php
/**
 * @file
 * Returns HTML for the footer region.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728118
 */
?>
<?php if ($content): ?>
  <footer class="<?php print $classes; ?>">
      <div class="container clearfix">
          <?php print $content; ?>
      </div>
  </footer>
<?php endif; ?>
