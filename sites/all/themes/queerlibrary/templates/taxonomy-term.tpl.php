<div id="taxonomy-term-<?php print $term->tid; ?>" class="<?php print $classes; ?>">

    <h2><?php print $term_name; ?></h2>


  <div class="content">
    <?php print render($content); ?>
  </div>

</div>
