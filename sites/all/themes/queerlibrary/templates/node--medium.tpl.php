<?php
/**
 * @file
 * Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
  field_titelbild,links,field_author_in,body,field_sprache,field_medientyp,field_lgbtiaqf
 */
hide($content['links']);
?>
<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    <?php print render($content['field_titelbild']); ?>
    <div class = "content">
        <?php print render($content); ?>
    </div>
    <?php if ($unpublished): ?>
        <mark class="unpublished"><?php print t('Unpublished'); ?></mark>
    <?php endif; ?>
</article>
