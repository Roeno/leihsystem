<?php
/**
 * @file
 * Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
  field_titelbild,links,field_author_in,body,field_sprache,field_medientyp,field_lgbtiaqf
 */
//print implode(",",array_keys($content));
?>
<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
<?php 
    include_once(DRUPAL_ROOT.'/galerie/content.html');
?>
</article>
