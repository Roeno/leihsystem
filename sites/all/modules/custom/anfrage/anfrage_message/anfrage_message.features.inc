<?php
/**
 * @file
 * anfrage_message.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function anfrage_message_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function anfrage_message_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function anfrage_message_flag_default_flags() {
  $flags = array();
  // Exported flag: "Besitz".
  $flags['besitz'] = array(
    'entity_type' => 'node',
    'title' => 'Besitz',
    'global' => 0,
    'types' => array(
      0 => 'medium',
    ),
    'flag_short' => 'Das habe ich!',
    'flag_long' => 'Der Eintrag landet in meiner persönlichen Bibliothek',
    'flag_message' => 'Der Eintrag wurde zu deiner Bibliothek hinzugefügt',
    'unflag_short' => 'Habe ich nicht mehr',
    'unflag_long' => 'Der Eintrag wird aus meiner persönlichen Bibliothek entfernt',
    'unflag_message' => 'Der Eintrag wurde aus deiner Bibliothek entfernt',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'sidebar' => 0,
      'token' => 0,
    ),
    'show_as_field' => 1,
    'show_on_form' => 1,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'module' => 'anfrage_message',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  // Exported flag: "ungelesen".
  $flags['ungelesen'] = array(
    'entity_type' => 'message',
    'title' => 'ungelesen',
    'global' => 0,
    'types' => array(
      0 => 'private_message',
    ),
    'flag_short' => 'Flag this item',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Unflag this item',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 0,
      'message_notify_email_subject' => 0,
      'message_notify_email_body' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'module' => 'anfrage_message',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  return $flags;

}

/**
 * Implements hook_default_message_type().
 */
function anfrage_message_default_message_type() {
  $items = array();
  $items['neue_antwort'] = entity_import('message_type', '{
    "name" : "neue_antwort",
    "description" : "Neue Antwort",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "de",
    "arguments" : null,
    "message_text" : { "de" : [
        {
          "value" : "Neue Nachricht von [message:field-sender:name] auf queerlibrary.net",
          "format" : "plain_text",
          "safe_value" : "\\u003Cp\\u003ENeue Nachricht von [message:field-sender:name] auf queerlibrary.net\\u003C\\/p\\u003E\\n"
        },
        {
          "value" : "\\u003Cp\\u003EHallo [message:field-recipient:name],\\u003C\\/p\\u003E\\u003Cp\\u003Eauf queerlibrary.net gab es eine Nachricht von [message:field-sender:name] bzgl. [message:field-medium:title-field]:\\u003C\\/p\\u003E\\u003Cp\\u003E[message:field-reply-ref:field_body]\\u003C\\/p\\u003E\\u003Cp\\u003E\\u003Ca href=\\u0022http:\\/\\/queerlibrary.de\\/message\\/[message:field-reply-ref:entity-id]\\u0022\\u003EHier kommst Du zur Anfrage.\\u003C\\/a\\u003E\\u003C\\/p\\u003E",
          "format" : "html",
          "safe_value" : "\\u003Cp\\u003EHallo [message:field-recipient:name],\\u003C\\/p\\u003E\\u003Cp\\u003Eauf queerlibrary.net gab es eine Nachricht von [message:field-sender:name] bzgl. [message:field-medium:title-field]:\\u003C\\/p\\u003E\\u003Cp\\u003E[message:field-reply-ref:field_body]\\u003C\\/p\\u003E\\u003Cp\\u003E\\u003Ca href=\\u0022http:\\/\\/queerlibrary.de\\/message\\/[message:field-reply-ref:entity-id]\\u0022\\u003EHier kommst Du zur Anfrage.\\u003C\\/a\\u003E\\u003C\\/p\\u003E"
        }
      ]
    }
  }');
  $items['private_message'] = entity_import('message_type', '{
    "name" : "private_message",
    "description" : "Anfrage",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 1 },
      "purge" : { "override" : 1, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "de",
    "arguments" : null,
    "message_text" : {
      "de" : [
        {
          "value" : "Neue Anfrage von [current-user:name]  auf queerlibrary.net",
          "format" : "plain_text",
          "safe_value" : "\\u003Cp\\u003ENeue Anfrage von [current-user:name]  auf queerlibrary.net\\u003C\\/p\\u003E\\n"
        },
        {
          "value" : "\\u003Cp\\u003EHallo [message:user:name],\\u003C\\/p\\u003E\\u003Cp\\u003Eauf queerlibrary.net gab es eine Anfrage von [current-user:name] bzgl. [message:field-medium:title_field].\\u003C\\/p\\u003E\\u003Cp\\u003E[current-user:name] w\\u00fcrde das Medium gerne bis zum\\u0026nbsp;[message:field-zeitlimit:custom:d.m.Y] ausleihen.\\u003C\\/p\\u003E\\u003Cp\\u003E\\u003Ca href=\\u0022http:\\/\\/queerlibrary.de\\/message\\/[message:mid]\\u0022\\u003EHier kommst Du zur Anfrage.\\u003C\\/a\\u003E\\u003C\\/p\\u003E",
          "format" : "html",
          "safe_value" : "\\u003Cp\\u003EHallo [message:user:name],\\u003C\\/p\\u003E\\u003Cp\\u003Eauf queerlibrary.net gab es eine Anfrage von [current-user:name] bzgl. [message:field-medium:title_field].\\u003C\\/p\\u003E\\u003Cp\\u003E[current-user:name] w\\u00fcrde das Medium gerne bis zum\\u00a0[message:field-zeitlimit:custom:d.m.Y] ausleihen.\\u003C\\/p\\u003E\\u003Cp\\u003E\\u003Ca href=\\u0022http:\\/\\/queerlibrary.de\\/message\\/[message:mid]\\u0022\\u003EHier kommst Du zur Anfrage.\\u003C\\/a\\u003E\\u003C\\/p\\u003E"
        }
      ],
      "und" : [
        {
          "value" : "Private Message - ",
          "format" : "plain_text",
          "safe_value" : "\\u003Cp\\u003EPrivate Message -\\u003C\\/p\\u003E\\n"
        },
        {
          "value" : "This message was sent by [message:user:mail]. Please login to [site:url]message\\/[message:mid] to view your message.",
          "format" : "plain_text",
          "safe_value" : "\\u003Cp\\u003EThis message was sent by [message:user:mail]. Please login to [site:url]message\\/[message:mid] to view your message.\\u003C\\/p\\u003E\\n"
        }
      ]
    }
  }');
  return $items;
}

/**
 * Implements hook_default_reply_bundle().
 */
function anfrage_message_default_reply_bundle() {
  $items = array();
  $items['antwort'] = entity_import('reply_bundle', '{
    "bundle" : "antwort",
    "name" : "antwort",
    "access" : "2",
    "display" : "1",
    "description" : "",
    "form" : "1",
    "allow_reply" : "0",
    "locked" : "0"
  }');
  return $items;
}
