<?php
/**
 * @file
 * anfrage_message.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function anfrage_message_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer mailsystem'.
  $permissions['administer mailsystem'] = array(
    'name' => 'administer mailsystem',
    'roles' => array(),
    'module' => 'mailsystem',
  );

  // Exported permission: 'administer phpmailer settings'.
  $permissions['administer phpmailer settings'] = array(
    'name' => 'administer phpmailer settings',
    'roles' => array(),
    'module' => 'phpmailer',
  );

  // Exported permission: 'edit mimemail user settings'.
  $permissions['edit mimemail user settings'] = array(
    'name' => 'edit mimemail user settings',
    'roles' => array(),
    'module' => 'mimemail',
  );

  // Exported permission: 'send arbitrary files'.
  $permissions['send arbitrary files'] = array(
    'name' => 'send arbitrary files',
    'roles' => array(),
    'module' => 'mimemail',
  );

  return $permissions;
}
