<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$query = new EntityFieldQuery();
$query->entityCondition('entity_type', 'message')
      ->entityCondition('bundle', 'private_message');
$result = $query->execute();
if (isset($result['message'])) {
  $message_items_nids = array_keys($result['message']);
  entity_delete_multiple('message', $message_items_nids);
}


$query = new EntityFieldQuery();
$query->entityCondition('entity_type', 'reply')
      ->entityCondition('bundle', 'antowrt');
$result = $query->execute();
if (isset($result['reply'])) {
  $message_items_nids = array_keys($result['reply']);
  entity_delete_multiple('reply', $message_items_nids);
}

$query = new EntityFieldQuery();
$query->entityCondition('entity_type', 'flagging')
      ->entityCondition('bundle', 'besitz');

$result = $query->execute();
if (isset($result['flagging'])) {
  $items_ids = array_keys($result['flagging']);
  $flags=entity_load('flagging',$items_ids);
  foreach ($flags as $flag) {
      if ($flag->field_status[LANGUAGE_NONE][0]['value']!='1') {
        dpm($flag);
        $flag->field_status[LANGUAGE_NONE][0]['value']='1';    
        entity_save('flagging',$flag);
      }
  }
}
