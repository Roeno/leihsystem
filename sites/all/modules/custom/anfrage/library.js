(function ($) {

    Drupal.behaviors.library = {
        attach: function (context, settings) {
            bezirkeLayer = document.getElementById("Bezirke");
            if (bezirkeLayer != null) {
                var bezirke = $('#Bezirke').children();
                $('.form-item-bezirk input').bind('click change', bindMapState);
                $('.form-item-bezirk input').each(bindMapState);
                for (i = 0; i < bezirke.length; i++) {

                    $(bezirke[i]).click(function () {

                        var input = document.getElementById('cb-' + this.getAttribute('id'));
                        if (input == null) {
                            console.log("Problem: " + this.getAttribute('id'));
                        } else {
                            input.checked = !input.checked;
                            $(input).change();
                        }
                    });
                }

            }
            
            /*
             * Setting added / delete Flaggs after flagging / unflagging
             */
            $(document).bind('flagGlobalAfterLinkUpdate', function(event, data) {
                if (data.flagStatus=='unflagged' && data.flagName=='besitz') {
                    if ($(data.link).parents('.views-row').length) {
                        $(data.link).parents('.views-row').eq(0).removeClass("mymedia").removeClass("added").addClass("deleted");
                    }
                }
                if (data.flagStatus=='flagged' && data.flagName=='besitz') {
                    if ($(data.link).parents('.views-row').length) {
                        $(data.link).parents('.views-row').eq(0).removeClass("deleted").addClass("mymedia").addClass("added");;
                    }
                }
            });
        }
    };
    function bindMapState() {
        this.setAttribute('id', 'cb-' + $(this).parent().text().trim());
        if (this.checked) {
            document.getElementById($(this).parent().text().trim()).setAttribute('class', 'bezirk checked');
        } else {
            document.getElementById($(this).parent().text().trim()).setAttribute('class', 'bezirk');
        }
    }
})(jQuery);