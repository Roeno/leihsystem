<?php
/**
 * @file
 * common_field_bases.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function common_field_bases_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'file-image-field_quelle_copyright'.
  $field_instances['file-image-field_quelle_copyright'] = array(
    'bundle' => 'image',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'file',
    'field_name' => 'field_quelle_copyright',
    'label' => 'Quelle / Copyright',
    'placeholder' => '',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'use_title_as_placeholder' => 0,
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 4,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Quelle / Copyright');

  return $field_instances;
}
