<?php
/**
 * @file
 * common_field_bases.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function common_field_bases_taxonomy_default_vocabularies() {
  return array(
    'themen' => array(
      'name' => 'Themen',
      'machine_name' => 'themen',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
