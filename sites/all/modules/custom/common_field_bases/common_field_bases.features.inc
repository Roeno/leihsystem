<?php
/**
 * @file
 * common_field_bases.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function common_field_bases_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function common_field_bases_image_default_styles() {
  $styles = array();

  // Exported image style: 300square.
  $styles['300square'] = array(
    'label' => '300square',
    'effects' => array(
      4 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 300,
          'height' => 300,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: thumb_square.
  $styles['thumb_square'] = array(
    'label' => 'thumb_square',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 95,
          'height' => 95,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: tiny.
  $styles['tiny'] = array(
    'label' => 'tiny',
    'effects' => array(
      3 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 32,
          'height' => 32,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
