<?php
/**
 * @file
 * user_profile.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function user_profile_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "entity_path" && $api == "entity_path") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function user_profile_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
