<?php
/**
 * @file
 * user_profile.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function user_profile_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access administration pages'.
  $permissions['access administration pages'] = array(
    'name' => 'access administration pages',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'system',
  );

  // Exported permission: 'access content'.
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'access content overview'.
  $permissions['access content overview'] = array(
    'name' => 'access content overview',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'node',
  );

  // Exported permission: 'access contextual links'.
  $permissions['access contextual links'] = array(
    'name' => 'access contextual links',
    'roles' => array(
      'Redaktion' => 'Redaktion',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'contextual',
  );

  // Exported permission: 'access site in maintenance mode'.
  $permissions['access site in maintenance mode'] = array(
    'name' => 'access site in maintenance mode',
    'roles' => array(),
    'module' => 'system',
  );

  // Exported permission: 'access user profiles'.
  $permissions['access user profiles'] = array(
    'name' => 'access user profiles',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'user',
  );

  // Exported permission: 'administer file types'.
  $permissions['administer file types'] = array(
    'name' => 'administer file types',
    'roles' => array(),
    'module' => 'file_entity',
  );

  // Exported permission: 'administer files'.
  $permissions['administer files'] = array(
    'name' => 'administer files',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'file_entity',
  );

  // Exported permission: 'administer nodes'.
  $permissions['administer nodes'] = array(
    'name' => 'administer nodes',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'node',
  );

  // Exported permission: 'administer permissions'.
  $permissions['administer permissions'] = array(
    'name' => 'administer permissions',
    'roles' => array(),
    'module' => 'user',
  );

  // Exported permission: 'administer replies'.
  $permissions['administer replies'] = array(
    'name' => 'administer replies',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'reply',
  );

  // Exported permission: 'administer taxonomy'.
  $permissions['administer taxonomy'] = array(
    'name' => 'administer taxonomy',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'administer users'.
  $permissions['administer users'] = array(
    'name' => 'administer users',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'user',
  );

  // Exported permission: 'bypass node access'.
  $permissions['bypass node access'] = array(
    'name' => 'bypass node access',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'node',
  );

  // Exported permission: 'bypass private message access control'.
  $permissions['bypass private message access control'] = array(
    'name' => 'bypass private message access control',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'message_private',
  );

  // Exported permission: 'cancel account'.
  $permissions['cancel account'] = array(
    'name' => 'cancel account',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'user',
  );

  // Exported permission: 'change own username'.
  $permissions['change own username'] = array(
    'name' => 'change own username',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'user',
  );

  // Exported permission: 'create a neue_anfrage message instance'.
  $permissions['create a neue_anfrage message instance'] = array(
    'name' => 'create a neue_anfrage message instance',
    'roles' => array(),
    'module' => 'message_ui',
  );

  // Exported permission: 'create a neue_antwort message instance'.
  $permissions['create a neue_antwort message instance'] = array(
    'name' => 'create a neue_antwort message instance',
    'roles' => array(),
    'module' => 'message_ui',
  );

  // Exported permission: 'create a private_message message instance'.
  $permissions['create a private_message message instance'] = array(
    'name' => 'create a private_message message instance',
    'roles' => array(),
    'module' => 'message_ui',
  );

  // Exported permission: 'create any message instance'.
  $permissions['create any message instance'] = array(
    'name' => 'create any message instance',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'create blog content'.
  $permissions['create blog content'] = array(
    'name' => 'create blog content',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create files'.
  $permissions['create files'] = array(
    'name' => 'create files',
    'roles' => array(),
    'module' => 'file_entity',
  );

  // Exported permission: 'create medium content'.
  $permissions['create medium content'] = array(
    'name' => 'create medium content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create messages'.
  $permissions['create messages'] = array(
    'name' => 'create messages',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'message',
  );

  // Exported permission: 'create splashpage content'.
  $permissions['create splashpage content'] = array(
    'name' => 'create splashpage content',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any blog content'.
  $permissions['delete any blog content'] = array(
    'name' => 'delete any blog content',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any medium content'.
  $permissions['delete any medium content'] = array(
    'name' => 'delete any medium content',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any splashpage content'.
  $permissions['delete any splashpage content'] = array(
    'name' => 'delete any splashpage content',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own blog content'.
  $permissions['delete own blog content'] = array(
    'name' => 'delete own blog content',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own medium content'.
  $permissions['delete own medium content'] = array(
    'name' => 'delete own medium content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own splashpage content'.
  $permissions['delete own splashpage content'] = array(
    'name' => 'delete own splashpage content',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete revisions'.
  $permissions['delete revisions'] = array(
    'name' => 'delete revisions',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in author_in'.
  $permissions['delete terms in author_in'] = array(
    'name' => 'delete terms in author_in',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in kategorie'.
  $permissions['delete terms in kategorie'] = array(
    'name' => 'delete terms in kategorie',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in lgbtiaq'.
  $permissions['delete terms in lgbtiaq'] = array(
    'name' => 'delete terms in lgbtiaq',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in medientyp'.
  $permissions['delete terms in medientyp'] = array(
    'name' => 'delete terms in medientyp',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in sprache'.
  $permissions['delete terms in sprache'] = array(
    'name' => 'delete terms in sprache',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in stadtteil'.
  $permissions['delete terms in stadtteil'] = array(
    'name' => 'delete terms in stadtteil',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in themen'.
  $permissions['delete terms in themen'] = array(
    'name' => 'delete terms in themen',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit a neue_anfrage message instance'.
  $permissions['edit a neue_anfrage message instance'] = array(
    'name' => 'edit a neue_anfrage message instance',
    'roles' => array(),
    'module' => 'message_ui',
  );

  // Exported permission: 'edit a neue_antwort message instance'.
  $permissions['edit a neue_antwort message instance'] = array(
    'name' => 'edit a neue_antwort message instance',
    'roles' => array(),
    'module' => 'message_ui',
  );

  // Exported permission: 'edit a private_message message instance'.
  $permissions['edit a private_message message instance'] = array(
    'name' => 'edit a private_message message instance',
    'roles' => array(),
    'module' => 'message_ui',
  );

  // Exported permission: 'edit any blog content'.
  $permissions['edit any blog content'] = array(
    'name' => 'edit any blog content',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any medium content'.
  $permissions['edit any medium content'] = array(
    'name' => 'edit any medium content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any message instance'.
  $permissions['edit any message instance'] = array(
    'name' => 'edit any message instance',
    'roles' => array(),
    'module' => 'message_ui',
  );

  // Exported permission: 'edit any splashpage content'.
  $permissions['edit any splashpage content'] = array(
    'name' => 'edit any splashpage content',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own blog content'.
  $permissions['edit own blog content'] = array(
    'name' => 'edit own blog content',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own medium content'.
  $permissions['edit own medium content'] = array(
    'name' => 'edit own medium content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own splashpage content'.
  $permissions['edit own splashpage content'] = array(
    'name' => 'edit own splashpage content',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in author_in'.
  $permissions['edit terms in author_in'] = array(
    'name' => 'edit terms in author_in',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in kategorie'.
  $permissions['edit terms in kategorie'] = array(
    'name' => 'edit terms in kategorie',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in lgbtiaq'.
  $permissions['edit terms in lgbtiaq'] = array(
    'name' => 'edit terms in lgbtiaq',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in medientyp'.
  $permissions['edit terms in medientyp'] = array(
    'name' => 'edit terms in medientyp',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in sprache'.
  $permissions['edit terms in sprache'] = array(
    'name' => 'edit terms in sprache',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in stadtteil'.
  $permissions['edit terms in stadtteil'] = array(
    'name' => 'edit terms in stadtteil',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in themen'.
  $permissions['edit terms in themen'] = array(
    'name' => 'edit terms in themen',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'revert revisions'.
  $permissions['revert revisions'] = array(
    'name' => 'revert revisions',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'node',
  );

  // Exported permission: 'select account cancellation method'.
  $permissions['select account cancellation method'] = array(
    'name' => 'select account cancellation method',
    'roles' => array(),
    'module' => 'user',
  );

  // Exported permission: 'use text format html'.
  $permissions['use text format html'] = array(
    'name' => 'use text format html',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'filter',
  );

  // Exported permission: 'view a neue_anfrage message instance'.
  $permissions['view a neue_anfrage message instance'] = array(
    'name' => 'view a neue_anfrage message instance',
    'roles' => array(),
    'module' => 'message_ui',
  );

  // Exported permission: 'view a neue_antwort message instance'.
  $permissions['view a neue_antwort message instance'] = array(
    'name' => 'view a neue_antwort message instance',
    'roles' => array(),
    'module' => 'message_ui',
  );

  // Exported permission: 'view a private_message message instance'.
  $permissions['view a private_message message instance'] = array(
    'name' => 'view a private_message message instance',
    'roles' => array(),
    'module' => 'message_ui',
  );

  // Exported permission: 'view antwort reply'.
  $permissions['view antwort reply'] = array(
    'name' => 'view antwort reply',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'reply',
  );

  // Exported permission: 'view any message instance'.
  $permissions['view any message instance'] = array(
    'name' => 'view any message instance',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'view own files'.
  $permissions['view own files'] = array(
    'name' => 'view own files',
    'roles' => array(),
    'module' => 'file_entity',
  );

  // Exported permission: 'view own private files'.
  $permissions['view own private files'] = array(
    'name' => 'view own private files',
    'roles' => array(),
    'module' => 'file_entity',
  );

  // Exported permission: 'view own unpublished content'.
  $permissions['view own unpublished content'] = array(
    'name' => 'view own unpublished content',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'node',
  );

  // Exported permission: 'view private files'.
  $permissions['view private files'] = array(
    'name' => 'view private files',
    'roles' => array(),
    'module' => 'file_entity',
  );

  // Exported permission: 'view revisions'.
  $permissions['view revisions'] = array(
    'name' => 'view revisions',
    'roles' => array(
      'Redaktion' => 'Redaktion',
    ),
    'module' => 'node',
  );

  // Exported permission: 'view the administration theme'.
  $permissions['view the administration theme'] = array(
    'name' => 'view the administration theme',
    'roles' => array(),
    'module' => 'system',
  );

  return $permissions;
}
