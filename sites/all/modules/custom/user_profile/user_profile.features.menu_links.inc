<?php
/**
 * @file
 * user_profile.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function user_profile_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: user-menu_eigene-medien:user/me/medien.
  $menu_links['user-menu_eigene-medien:user/me/medien'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user/me/medien',
    'router_path' => 'user/me/medien',
    'link_title' => 'Eigene Medien',
    'options' => array(
      'identifier' => 'user-menu_eigene-medien:user/me/medien',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: user-menu_log-out:user/logout.
  $menu_links['user-menu_log-out:user/logout'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user/logout',
    'router_path' => 'user/logout',
    'link_title' => 'Log out',
    'options' => array(
      'identifier' => 'user-menu_log-out:user/logout',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: user-menu_mein-account:user.
  $menu_links['user-menu_mein-account:user'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user',
    'router_path' => 'user',
    'link_title' => 'Mein Account',
    'options' => array(
      'alter' => TRUE,
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'user-menu_mein-account:user',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: user-menu_nachrichten:message/all.
  $menu_links['user-menu_nachrichten:message/all'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'message/all',
    'router_path' => 'message/all',
    'link_title' => 'Nachrichten',
    'options' => array(
      'identifier' => 'user-menu_nachrichten:message/all',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Eigene Medien');
  t('Log out');
  t('Mein Account');
  t('Nachrichten');

  return $menu_links;
}
