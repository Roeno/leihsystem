<?php
/**
 * @file
 * user_profile.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function user_profile_user_default_roles() {
  $roles = array();

  // Exported role: Redaktion.
  $roles['Redaktion'] = array(
    'name' => 'Redaktion',
    'weight' => 2,
  );

  return $roles;
}
