<?php
/**
 * @file
 * user_profile.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function user_profile_taxonomy_default_vocabularies() {
  return array(
    'stadtteil' => array(
      'name' => 'Bezirke',
      'machine_name' => 'stadtteil',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
