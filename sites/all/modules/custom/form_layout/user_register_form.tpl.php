 
<div id="content" class="column" role="main">

    <?php print render($content); ?>
    <?php if ($form): ?>
        <?php print drupal_render_children($form); ?>
    <?php endif; ?>

</div>
<?php
// Render the sidebars to see if there's anything in them.
$sidebar_first = render($sidebar_first);
$sidebar_second = render($sidebar_second);
?>

<?php if ($sidebar_first || $sidebar_second): ?>
    <aside class="sidebars">
        <section class="region region-sidebar-first column sidebar">
            <?php print $sidebar_first; ?>
        </section>
        <section class="region region-sidebar-second column sidebar">
            <?php print $sidebar_second; ?>
        </section>
    </aside>
<?php endif; ?>