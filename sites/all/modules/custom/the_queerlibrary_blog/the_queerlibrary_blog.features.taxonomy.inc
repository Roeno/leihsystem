<?php
/**
 * @file
 * the_queerlibrary_blog.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function the_queerlibrary_blog_taxonomy_default_vocabularies() {
  return array(
    'themen' => array(
      'name' => 'Themen',
      'machine_name' => 'themen',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
