<?php
/**
 * @file
 * queer_library_blocks.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function queer_library_blocks_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['menu-menu-projektmenue'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu-projektmenue',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'queerlibrary',
        'weight' => -5,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['menu_block-1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 1,
    'module' => 'menu_block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => 'navigation',
        'status' => 1,
        'theme' => 'queerlibrary',
        'weight' => -21,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['menu_block-3'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 3,
    'module' => 'menu_block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'queerlibrary',
        'weight' => -15,
      ),
    ),
    'title' => 'The Queer Library',
    'visibility' => 0,
  );

  $export['system-user-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'user-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'queerlibrary',
        'weight' => -2,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['user-login'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'login',
    'module' => 'user',
    'node_types' => array(),
    'pages' => '*',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => 'navigation',
        'status' => 1,
        'theme' => 'queerlibrary',
        'weight' => -20,
      ),
    ),
    'title' => 'Login',
    'visibility' => 1,
  );

  $export['views--exp-library-page'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '-exp-library-page',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'library
library/*',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'queerlibrary',
        'weight' => -17,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views--exp-library-page_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '-exp-library-page_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'user/*/medien',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'queerlibrary',
        'weight' => -16,
      ),
    ),
    'title' => 'Eigene Medien',
    'visibility' => 1,
  );

  $export['views--exp-library-page_2'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '-exp-library-page_2',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'users/*',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'queerlibrary',
        'weight' => -13,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['views-antworten-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'antworten-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'message/*',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'queerlibrary',
        'weight' => -20,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-nachrichten-block_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'nachrichten-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'message/all
message/*',
    'roles' => array(
      'authenticated user' => 2,
    ),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'queerlibrary',
        'weight' => -12,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-nachrichten-block_2'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'nachrichten-block_2',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'queerlibrary',
        'weight' => -1,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-nachrichten-block_3'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'nachrichten-block_3',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'user/me',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'queerlibrary',
        'weight' => -17,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-neues-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'neues-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>
blog
blog/*',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'queerlibrary',
        'weight' => -20,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-neues-block_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'neues-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>
blog/*
blog',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'queerlibrary',
        'weight' => -21,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-neues-block_2'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'neues-block_2',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'queerlibrary',
        'weight' => -21,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-neues-block_3'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'neues-block_3',
    'module' => 'views',
    'node_types' => array(
      0 => 'blog',
      1 => 'medium',
    ),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'queerlibrary',
        'weight' => -14,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-neues-block_4'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'neues-block_4',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => 'content_bottom',
        'status' => 1,
        'theme' => 'queerlibrary',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-neues-block_5'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'neues-block_5',
    'module' => 'views',
    'node_types' => array(
      0 => 'medium',
    ),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'queerlibrary',
        'weight' => -18,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-neues-block_6'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'neues-block_6',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>
blog
blog/*',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'queerlibrary',
        'weight' => -13,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-tag_description-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'tag_description-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'taxonomy/term/*',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'queerlibrary',
        'weight' => -18,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-tag_description-block_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'tag_description-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>
authorin/*',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'queerlibrary',
        'weight' => -19,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-user_profil-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'user_profil-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'user/*',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'queerlibrary' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'queerlibrary',
        'weight' => -20,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  return $export;
}
