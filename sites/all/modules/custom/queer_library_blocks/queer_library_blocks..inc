<?php
/**
 * @file
 * queer_library_blocks..inc
 */

/**
 * Implements hook_default_block_machine_name_boxes().
 */
function queer_library_blocks_default_block_machine_name_boxes() {
  $export = array();

  $block_machine_name_boxes = new stdClass();
  $block_machine_name_boxes->delta = '-exp-library-page_2';
  $block_machine_name_boxes->machine_name = 'filter_others';
  $block_machine_name_boxes->module = 'views';
  $export['filter_others'] = $block_machine_name_boxes;

  $block_machine_name_boxes = new stdClass();
  $block_machine_name_boxes->delta = '-exp-library-page_1';
  $block_machine_name_boxes->machine_name = 'filter_own';
  $block_machine_name_boxes->module = 'views';
  $export['filter_own'] = $block_machine_name_boxes;

  $block_machine_name_boxes = new stdClass();
  $block_machine_name_boxes->delta = '-exp-library-page';
  $block_machine_name_boxes->machine_name = 'filterslibrary';
  $block_machine_name_boxes->module = 'views';
  $export['filterslibrary'] = $block_machine_name_boxes;

  $block_machine_name_boxes = new stdClass();
  $block_machine_name_boxes->delta = '1';
  $block_machine_name_boxes->machine_name = 'footer_text_queerlibrary';
  $block_machine_name_boxes->module = 'block';
  $export['footer_text_queerlibrary'] = $block_machine_name_boxes;

  $block_machine_name_boxes = new stdClass();
  $block_machine_name_boxes->delta = '2';
  $block_machine_name_boxes->machine_name = 'menu_2nd_level';
  $block_machine_name_boxes->module = 'menu_block';
  $export['menu_2nd_level'] = $block_machine_name_boxes;

  $block_machine_name_boxes = new stdClass();
  $block_machine_name_boxes->delta = 'nachrichten-block_1';
  $block_machine_name_boxes->machine_name = 'message_listbox';
  $block_machine_name_boxes->module = 'views';
  $export['message_listbox'] = $block_machine_name_boxes;

  $block_machine_name_boxes = new stdClass();
  $block_machine_name_boxes->delta = 'tag_description-block_1';
  $block_machine_name_boxes->machine_name = 'neue_autorinnen';
  $block_machine_name_boxes->module = 'views';
  $export['neue_autorinnen'] = $block_machine_name_boxes;

  $block_machine_name_boxes = new stdClass();
  $block_machine_name_boxes->delta = 'neues-block';
  $block_machine_name_boxes->machine_name = 'neue_medien';
  $block_machine_name_boxes->module = 'views';
  $export['neue_medien'] = $block_machine_name_boxes;

  $block_machine_name_boxes = new stdClass();
  $block_machine_name_boxes->delta = 'neues-block_6';
  $block_machine_name_boxes->machine_name = 'neuethemen';
  $block_machine_name_boxes->module = 'views';
  $export['neuethemen'] = $block_machine_name_boxes;

  $block_machine_name_boxes = new stdClass();
  $block_machine_name_boxes->delta = 'neues-block_1';
  $block_machine_name_boxes->machine_name = 'neuimblock';
  $block_machine_name_boxes->module = 'views';
  $export['neuimblock'] = $block_machine_name_boxes;

  $block_machine_name_boxes = new stdClass();
  $block_machine_name_boxes->delta = '3';
  $block_machine_name_boxes->machine_name = 'projektmenue';
  $block_machine_name_boxes->module = 'menu_block';
  $export['projektmenue'] = $block_machine_name_boxes;

  $block_machine_name_boxes = new stdClass();
  $block_machine_name_boxes->delta = 'neues-block_2';
  $block_machine_name_boxes->machine_name = 'referenziertemedien';
  $block_machine_name_boxes->module = 'views';
  $export['referenziertemedien'] = $block_machine_name_boxes;

  $block_machine_name_boxes = new stdClass();
  $block_machine_name_boxes->delta = 'neues-block_4';
  $block_machine_name_boxes->machine_name = 'rezensionen';
  $block_machine_name_boxes->module = 'views';
  $export['rezensionen'] = $block_machine_name_boxes;

  $block_machine_name_boxes = new stdClass();
  $block_machine_name_boxes->delta = 'tag_description-block';
  $block_machine_name_boxes->machine_name = 'term_sidebar';
  $block_machine_name_boxes->module = 'views';
  $export['term_sidebar'] = $block_machine_name_boxes;

  $block_machine_name_boxes = new stdClass();
  $block_machine_name_boxes->delta = 'neues-block_3';
  $block_machine_name_boxes->machine_name = 'titelbild';
  $block_machine_name_boxes->module = 'views';
  $export['titelbild'] = $block_machine_name_boxes;

  $block_machine_name_boxes = new stdClass();
  $block_machine_name_boxes->delta = 'user_profil-block';
  $block_machine_name_boxes->machine_name = 'userprofil';
  $block_machine_name_boxes->module = 'views';
  $export['userprofil'] = $block_machine_name_boxes;

  $block_machine_name_boxes = new stdClass();
  $block_machine_name_boxes->delta = 'neues-block_5';
  $block_machine_name_boxes->machine_name = 'verfuegbarkeit';
  $block_machine_name_boxes->module = 'views';
  $export['verfuegbarkeit'] = $block_machine_name_boxes;

  $block_machine_name_boxes = new stdClass();
  $block_machine_name_boxes->delta = 'nachrichten-block_3';
  $block_machine_name_boxes->machine_name = 'verliehen_liste';
  $block_machine_name_boxes->module = 'views';
  $export['verliehen_liste'] = $block_machine_name_boxes;

  return $export;
}
