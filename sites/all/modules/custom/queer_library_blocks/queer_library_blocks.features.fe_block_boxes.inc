<?php
/**
 * @file
 * queer_library_blocks.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function queer_library_blocks_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'THE QUEER LIBRARY Footer';
  $fe_block_boxes->format = 'html';
  $fe_block_boxes->machine_name = 'footer_text_queerlibrary';
  $fe_block_boxes->body = '<address>QUEER LIBRARY e.V.</address><p>info@queerlibrary.de</p>';

  $export['footer_text_queerlibrary'] = $fe_block_boxes;

  return $export;
}
