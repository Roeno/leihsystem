<?php
/**
 * @file
 * media_and_library.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function media_and_library_taxonomy_default_vocabularies() {
  return array(
    'author_in' => array(
      'name' => 'Autor_in',
      'machine_name' => 'author_in',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'kategorie' => array(
      'name' => 'Kategorie',
      'machine_name' => 'kategorie',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'lgbtiaq' => array(
      'name' => 'Rubrik',
      'machine_name' => 'lgbtiaq',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'medientyp' => array(
      'name' => 'Medientyp',
      'machine_name' => 'medientyp',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'sprache' => array(
      'name' => 'Sprache',
      'machine_name' => 'sprache',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
