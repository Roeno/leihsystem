<?php
/**
 * @file
 * media_and_library.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function media_and_library_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'flag besitz'.
  $permissions['flag besitz'] = array(
    'name' => 'flag besitz',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'unflag besitz'.
  $permissions['unflag besitz'] = array(
    'name' => 'unflag besitz',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'flag',
  );

  return $permissions;
}
